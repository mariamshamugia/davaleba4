package com.example.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            val toast = Toast.makeText(this, "CLICK ON THE ANSWERS_BUTTON, IF U WANT TO SEE A RESULT " , Toast.LENGTH_SHORT)
            toast.show()
        }

        var nav = findViewById<BottomNavigationView>(R.id.buttonNavMenu)
        var controller = findNavController(R.id.nav_host_fragment_container)

        var appBarConfiguration = AppBarConfiguration(
            setOf(
                R.layout.fragment_questions,
                R.layout.fragment_answers)
        )

        setupActionBarWithNavController(controller,appBarConfiguration)
        nav.setupWithNavController(controller)
    }
}